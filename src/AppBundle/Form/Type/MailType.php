<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Mail;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'recipients',
                CollectionType::class,
                [
                    'entry_type' => RecipientType::class,
                    'allow_add' => true,
                    'by_reference' => false
                ]
            )
            ->add('sender')
            ->add('subject')
            ->add('body')
            ->add('priority', null, ['required' => false])
            ->add('attachmentFile', FileType::class, ['required' => false]);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Mail::class,
            'cascade_validation' => true,
            'csrf_protection' => false
        ]);
    }

}