<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SendCommand extends ContainerAwareCommand
{
    const SENT_EMAIL_LIMIT = 10;

    protected function configure()
    {
        $this
            ->setName('app:send')
            ->setDescription('Process mail queue')
            ->addOption(
                'limit',
                'l',
                InputOption::VALUE_OPTIONAL,
                'Number of emails to sent in one iteration',
                self::SENT_EMAIL_LIMIT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Process mail queue');

        $mailerService = $this->getContainer()->get('app.mail.service');

        $sentMails = $mailerService->sent($input->getOption('limit'));

        if (0 === count($sentMails)) {
            $io->success('No messages to sent');
            return;
        }

        $headers = ['Sender', 'Recipients', 'Subject'];

        $rows = [];

        /** @var \AppBundle\Entity\Mail $mail */
        foreach ($sentMails as $mail) {
            $recipients = [];
            foreach ($mail->getRecipients() as $recipient) {
                $recipients[] = $recipient->getAddress();
            }

            $rows[] = [
                $mail->getSender(),
                implode(', ', $recipients),
                $mail->getSubject()
            ];
        }

        $io->table($headers, $rows);

        $io->success('Messages sent');
    }
}