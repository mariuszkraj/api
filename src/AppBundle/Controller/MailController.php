<?php

namespace AppBundle\Controller;

use AppBundle\Exception\FormErrorException;
use AppBundle\Service\MailService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class MailController extends FOSRestController
{
    /**
     * @ApiDoc(
     *     description="Get list of Mail"
     * )
     */
    public function getMailsAction()
    {
        $mails = $this->get('app.mail.service')->getAll();

        return $mails;
    }

    /**
     * @ApiDoc(
     *     description="Get one Mail",
     *     output="AppBundle\Entity\Mail",
     * )
     */
    public function getMailAction($id)
    {
        $mail = $this->get('app.mail.service')->getOne($id);

        if (null === $mail) {
            throw new NotFoundHttpException('Mail not found');
        }

        return $mail;
    }

    /**
     * @ApiDoc(
     *     description="Create a new Mail",
     *     input="AppBundle\Entity\Mail",
     *     output="AppBundle\Entity\Mail",
     * )
     */
    public function postMailAction(Request $request)
    {
        $mailService = $this->get('app.mail.service');

        $data = $request->request->all();
        $data += $request->files->all();

        try {
            $form = $mailService->createNewForm();

            $mail = $mailService->processForm($form, $data);

            return View::create($mail, Response::HTTP_CREATED);

        } catch (FormErrorException $e) {
            return View::create($e->getErrors(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @ApiDoc(
     *     description="Send emails by ids",
     *     parameters={
     *         {"name"="ids", "dataType"="array", "required"=true, "description"="mail ids"}
     *     }
     * )
     */
    public function postSendAction(Request $request)
    {
        $mailIds = $request->request->get('ids', false);

        if (false === $mailIds) {
            throw new \HttpInvalidParamException();
        }

        $sentMails = $this->get('app.mail.service')->send(MailService::SENT_LIMIT, $mailIds);

        return $sentMails;
    }

    /**
     * @ApiDoc(
     *     description="Get api stats"
     * )
     */
    public function getStatsAction()
    {
        $mailService = $this->get('app.mail.service');

        return $mailService->getStats();
    }
}
