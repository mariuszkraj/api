<?php

namespace AppBundle\Service;

use AppBundle\Entity\Mail;
use AppBundle\Exception\FormErrorException;
use AppBundle\Form\Type\MailType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class MailService
{
    const STATS_LATEST_LIMIT = 10;
    const SENT_LIMIT = 10;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var string
     */
    private $attachmentUploadDir;

    /**
     * MailService constructor.
     * @param \Swift_Mailer $mailer
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $em
     */
    public function __construct(
        \Swift_Mailer $mailer,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $em,
        $attachmentUploadDir
    ) {
        $this->mailer = $mailer;
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->attachmentUploadDir = $attachmentUploadDir;
    }

    public function createNew()
    {
        return new Mail();
    }

    /**
     * @return string
     */
    public function getFormType()
    {
        return MailType::class;
    }

    public function createForm(Mail $mail)
    {
        return $this->formFactory->create($this->getFormType(), $mail);
    }

    public function createNewForm()
    {
        return $this->createForm($this->createNew());
    }

    /**
     * @param FormInterface $form
     * @param array $data
     *
     * @return Mail
     * @throws FormErrorException
     */
    public function processForm(FormInterface $form, array $data)
    {
        $form->submit($data);

        if ($form->isValid()) {
            $mail = $form->getData();

            $this->uploadAttachment($mail);

            $this->em->persist($mail);
            $this->em->flush();

            return $mail;
        }

        throw FormErrorException::factory($form);
    }

    private function uploadAttachment(Mail $mail)
    {
        if (null === $mail->getAttachmentFile()) {
            return;
        }

        $mail->getAttachmentFile()->move(
            $this->attachmentUploadDir,
            $mail->getAttachmentFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $mail->setAttachmentName($mail->getAttachmentFile()->getClientOriginalName());

        // clean up the file property as you won't need it anymore
        $mail->setAttachmentFile(null);
    }

    public function getAll()
    {
        return $this->em->getRepository('AppBundle:Mail')->findAll();
    }

    public function getWaitingByPriority($limit = null)
    {
        return $this->em->getRepository('AppBundle:Mail')->findBy(
            ['state' => Mail::STATE_WAITING],
            ['priority' => 'ASC'],
            $limit
        );
    }

    public function getWaitingByIds($ids)
    {
        return $this->em->getRepository('AppBundle:Mail')->findByIds($ids);
    }

    public function getOne($id)
    {
        return $this->em->getRepository('AppBundle:Mail')->find($id);
    }

    public function getStats()
    {
        return [
            'sent' => $this->em->getRepository('AppBundle:Mail')->sentCount(),
            'waiting' => $this->em->getRepository('AppBundle:Mail')->waitingCount(),
            'latestSent' => $this->em->getRepository('AppBundle:Mail')->findBy(
                ['state' => Mail::STATE_SENT],
                ['sentAt' => 'DESC'],
                self::STATS_LATEST_LIMIT
            ),
        ];
    }

    public function send($limit = self::SENT_LIMIT, $ids = [])
    {
        if (empty($ids)) {
            $mails = $this->getWaitingByPriority($limit);
        } else {
            $mails = $this->getWaitingByIds($ids);
        }

        foreach ($mails as $mail) {
            if (Mail::STATE_SENT === $mail->getState()) {
                continue;
            }

            $message = \Swift_Message::newInstance();

            $message->setFrom($mail->getSender());

            foreach ($mail->getRecipients() as $recipient) {
                $message->addTo($recipient->getAddress(), $recipient->getName());
            }

            $message->setSubject($mail->getSubject());
            $message->setBody($mail->getBody());

            $this->mailer->send($message);

            $mail->markSent();
            $this->em->persist($mail);
        }

        $this->em->flush();

        return $mails;
    }
}