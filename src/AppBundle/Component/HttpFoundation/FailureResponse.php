<?php

namespace AppBundle\Component\HttpFoundation;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class FailureResponse extends JsonResponse
{
    /**
     * @inheritDoc
     */
    public function __construct($data, array $headers = [])
    {
        parent::__construct(
            [
                'status' => false,
                'errors' => $data
            ],
            Response::HTTP_BAD_REQUEST,
            $headers
        );
    }

}