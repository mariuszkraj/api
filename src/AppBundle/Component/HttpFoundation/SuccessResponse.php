<?php

namespace AppBundle\Component\HttpFoundation;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SuccessResponse extends JsonResponse
{
    /**
     * @inheritDoc
     */
    public function __construct($data, array $headers = [])
    {
        parent::__construct(
            [
                'status' => true,
                'data' => $data
            ],
            Response::HTTP_OK,
            $headers
        );
    }

}