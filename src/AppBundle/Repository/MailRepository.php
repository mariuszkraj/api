<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Mail;
use Doctrine\ORM\EntityRepository;

class MailRepository extends EntityRepository
{
    private function countByState($state)
    {
        $query = $this->createQueryBuilder('m');
        $query
            ->select('COUNT(m)')
            ->where('m.state = :state')

            ->setParameter('state', $state);

        $query = $query->getQuery();
        $query->execute();

        return $query->getSingleScalarResult();
    }

    public function sentCount()
    {
        return $this->countByState(Mail::STATE_SENT);
    }

    public function waitingCount()
    {
        return $this->countByState(Mail::STATE_WAITING);
    }

    public function findByIds($ids)
    {
        foreach ($ids as &$id) {
            $id = (int) $id;
        }

        $query = $this->createQueryBuilder('m');
        $query
            ->select('m')
            ->where('m.id IN (:ids)')

            ->setParameter('ids', $ids);

        $query = $query->getQuery();
        $query->execute();

        return $query->getResult();
    }
}
