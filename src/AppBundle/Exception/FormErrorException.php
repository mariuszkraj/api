<?php

namespace AppBundle\Exception;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

class FormErrorException extends \Exception
{
    private $errors;

    public static function factory(FormInterface $form)
    {
        $exception = new FormErrorException();
        $exception->setErrors($form);

        return $exception;
    }

    public function setErrors(FormInterface $form)
    {
        $formErrors = $form->getErrors(true, true);

        $array = [];

        /** @var FormError $error */
        foreach ($formErrors as $error) {
            $field = $error->getOrigin()->getName();

            if (!isset($array[$field])) {
                $array[$field] = [];
            }

            $array[$field][] = $error->getMessage();
        }

        $this->errors = $array;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}