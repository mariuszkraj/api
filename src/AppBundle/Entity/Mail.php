<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Mail
 *
 * @ORM\Table(name="mail")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MailRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Mail
{
    const STATE_SENT = 1;
    const STATE_WAITING = 0;

    const PRIORITY_LOW = 100;
    const PRIORITY_MEDIUM = 50;
    const PRIORITY_HIGH = 1;

    static $states = [
        self::STATE_SENT,
        self::STATE_WAITING
    ];

    static $priorities = [
        self::PRIORITY_LOW,
        self::PRIORITY_MEDIUM,
        self::PRIORITY_HIGH
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Count(min="1")
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Recipient", mappedBy="mail", cascade={"persist"})
     */
    private $recipients;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @ORM\Column(name="sender", type="string")
     */
    private $sender;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(name="body", type="text")
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sentAt", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var bool
     *
     * @Assert\Choice(callback="getStates")
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var int
     *
     * @Assert\Choice(callback="getPriorities")
     *
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority;

    /**
     * @var File
     */
    private $attachmentFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $attachmentName;

    /**
     * Mail constructor.
     */
    public function __construct()
    {
        $this->recipients = new ArrayCollection();

        $this->state = self::STATE_WAITING;
        $this->priority = self::PRIORITY_LOW;

        $this->createdAt  = $this->updatedAt = new \DateTime();
    }

    public static function getStates()
    {
        return self::$states;
    }


    public static function getPriorities()
    {
        return self::$priorities;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ArrayCollection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param ArrayCollection $recipients
     * @return Mail
     */
    public function setRecipients(ArrayCollection $recipients)
    {
        $this->recipients = $recipients;

        foreach ($this->recipients as $recipient) {
            $recipient->setMail($this);
        }

        return $this;
    }

    public function addRecipient(Recipient $recipient)
    {
        if (!$this->recipients->contains($recipient)) {
            $recipient->setMail($this);
            $this->recipients->add($recipient);
        }
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     * @return Mail
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Mail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Mail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @return Mail
     */
    public function markSent()
    {
        $this->state = self::STATE_SENT;
        $this->sentAt = new \DateTime();
        return $this;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return Mail
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return bool
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return Mail
     */
    public function setPriority($priority)
    {
        if (in_array($priority, Mail::$priorities)) {
            $this->priority = $priority;
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getAttachmentFile()
    {
        return $this->attachmentFile;
    }

    /**
     * @param File $attachmentFile
     * @return Mail
     */
    public function setAttachmentFile(File $attachmentFile = null)
    {
        $this->attachmentFile = $attachmentFile;

        if ($attachmentFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getAttachmentName()
    {
        return $this->attachmentName;
    }

    /**
     * @param string $attachmentName
     * @return Mail
     */
    public function setAttachmentName($attachmentName)
    {
        $this->attachmentName = $attachmentName;
        return $this;
    }
}

