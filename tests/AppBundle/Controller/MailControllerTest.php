<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Mail;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class MailControllerTest extends WebTestCase
{
    protected function assertJsonResponse(Response $response, $statusCode = Response::HTTP_OK)
    {
        $this->assertEquals(
            $statusCode,
            $response->getStatusCode(),
            $response->getContent()
        );
        $this->assertTrue(
            $response->headers->contains('Content-Type', 'application/json'),
            $response->headers
        );
    }

    private function assertMail(array $mail)
    {
        $this->assertArrayHasKey('id', $mail);
        $this->assertArrayHasKey('recipients', $mail);
        $this->assertArrayHasKey('sender', $mail);
        $this->assertArrayHasKey('subject', $mail);
        $this->assertArrayHasKey('body', $mail);
        $this->assertArrayHasKey('priority', $mail);
        $this->assertArrayHasKey('state', $mail);
        $this->assertArrayHasKey('created_at', $mail);
        $this->assertArrayHasKey('sent_at', $mail);
        $this->assertArrayHasKey('attachment_name', $mail);
    }

    public function testGetMails()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/mails'
        );

        $this->assertJsonResponse($client->getResponse());

        $response = json_decode($client->getResponse()->getContent(), true);

        foreach ($response as $mail) {
            $this->assertMail($mail);
        }
    }

    public function testGetMail()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/mails/1'
        );

        $this->assertJsonResponse($client->getResponse());

        $mail = json_decode($client->getResponse()->getContent(), true);

        $this->assertMail($mail);
    }

    public function testPostMail()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/mails',
            [
                'recipients' => [
                    ['address' => 'user@example.com', 'name' => 'dummy']
                ],
                'sender' => 'sender@example.com',
                'subject' => 'Dummy email to example user',
                'body' => 'Dummy body of dummy email to example user'
            ]
        );

        $this->assertJsonResponse($client->getResponse(), Response::HTTP_CREATED);

        $mail = json_decode($client->getResponse()->getContent(), true);

        $this->assertMail($mail);
    }

    public function testPostMailLackOfData()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/mails',
            []
        );

        $this->assertJsonResponse($client->getResponse(), Response::HTTP_BAD_REQUEST);

        $errors = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('recipients', $errors);
        $this->assertArrayHasKey('sender', $errors);
        $this->assertArrayHasKey('subject', $errors);
        $this->assertArrayHasKey('body', $errors);
    }

    public function testPostMailsMultipleRecipient()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/mails',
            [
                'recipients' => [
                    ['address' => 'user@example.com'],
                    ['address' => 'admin@example.com']
                ],
                'sender' => 'sender@exaple.com',
                'subject' => 'Dummy email to example user',
                'body' => 'Dummy body of dummy email to example user'
            ]
        );

        $this->assertJsonResponse($client->getResponse(), Response::HTTP_CREATED);

        $mail = json_decode($client->getResponse()->getContent(), true);

        $this->assertMail($mail);
    }

    public function testPostMailHighPriority()
    {
        $client = static::createClient();

        $client->request(
            'POST',
            '/api/mails',
            [
                'recipients' => [
                    ['address' => 'user@example.com', 'name' => 'dummy']
                ],
                'sender' => 'sender@example.com',
                'subject' => 'Dummy email to example user',
                'body' => 'Dummy body of dummy email to example user',
                'priority' => Mail::PRIORITY_HIGH
            ]
        );

        $this->assertJsonResponse($client->getResponse(), Response::HTTP_CREATED);

        $mail = json_decode($client->getResponse()->getContent(), true);

        $this->assertMail($mail);

        $this->assertEquals(Mail::PRIORITY_HIGH, $mail['priority']);
    }

    public function testPostMailWithAttachment()
    {
        $client = static::createClient();

        $sourceFile = __DIR__ . DIRECTORY_SEPARATOR . 'Dummy.jpg';

        $dummyName = uniqid() . '_Dummy.jpg';
        $dummyFile = __DIR__ . DIRECTORY_SEPARATOR . $dummyName;

        copy($sourceFile, $dummyFile);

        $file = new UploadedFile(
            $dummyFile,
            $dummyName,
            'image/jpg',
            9327000
        );

        $client->request(
            'POST',
            '/api/mails',
            [
                'recipients' => [
                    ['address' => 'user@example.com', 'name' => 'dummy']
                ],
                'sender' => 'sender@example.com',
                'subject' => 'Dummy email to example user',
                'body' => 'Dummy body of dummy email to example user',
                'priority' => Mail::PRIORITY_MEDIUM
            ],
            [
                'attachmentFile' => $file
            ]
        );

        $this->assertJsonResponse($client->getResponse(), Response::HTTP_CREATED);

        $mail = json_decode($client->getResponse()->getContent(), true);

        $this->assertMail($mail);

        $this->assertEquals(Mail::PRIORITY_MEDIUM, $mail['priority']);
        $this->assertNotNull($mail['attachment_name']);
        $this->assertContains('Dummy.jpg', $mail['attachment_name']);
    }

    public function testGetStats()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/stats'
        );

        $this->assertJsonResponse($client->getResponse());

        $stats = json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('sent', $stats);
        $this->assertArrayHasKey('waiting', $stats);
        $this->assertArrayHasKey('latestSent', $stats);

        foreach ($stats['latestSent'] as $mail) {
            $this->assertMail($mail);
        }
    }
}
