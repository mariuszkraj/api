API
===

# Most important components

* MailController (src/AppBundle/Controller/MailController)
* MailControllerTest (test/AppBundle/Controller/MailControllerTest)
* MailService (src/AppBundle/Service/MailService)
* MailRepository (src/AppBundle/Repository/MailRepository)
* MailType (src/AppBundle/Form/Type/MailType)
* SendCommand (src/AppBundle/Command/SendCommand)